#https://github.com/CentOS/CentOS-Dockerfiles
#https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
FROM centos:centos7.8.2003 as production
ARG PDCI_BASE_COMMIT_SHORT_SHA
#ARG PDCI_COMMIT_MESSAGE
ARG PDCI_BASE_COMMIT_TAG
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor=""
LABEL PDCI_BASE_COMMIT_TAG=${PDCI_BASE_COMMIT_SHORT_SHA}
LABEL PDCI_BASE_COMMIT_SHORT_SHA=${PDCI_BASE_COMMIT_TAG}
#LABEL License=GPLv2
#LABEL Version=0.0.1
#LABEL Features="1 melhoria x meria y"
#LABEL tag=""

COPY icmbio-ca/CA_ICMBIO.crt /etc/pki/ca-trust/source/anchors/CA_ICMBIO.crt
COPY serpro-ca/root_1.crt /etc/pki/ca-trust/source/anchors/root_1.crt
COPY serpro-ca/root_2.crt /etc/pki/ca-trust/source/anchors/root_2.crt
COPY serpro-ca/root_3.crt /etc/pki/ca-trust/source/anchors/root_3.crt
COPY serpro-ca/icpbrasilv10.crt /etc/pki/ca-trust/source/anchors/icpbrasilv10.crt
COPY serpro-ca/serprossl.crt /etc/pki/ca-trust/source/anchors/serprossl.crt
COPY lets-ca/lets-wild-icmbio-chain.pem /etc/pki/ca-trust/source/anchors/lets-wild-icmbio-chain.pem
RUN update-ca-trust
RUN update-ca-trust enable

#RUN yum --setopt=tsflags=nodocs update -y
#RUN yum  clean all
RUN yum --setopt=tsflags=nodocs install epel-release -y && \
    yum --setopt=tsflags=nodocs install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y  && \
    yum clean all && \
    rm -rf /var/cache/yum

#Instalacao do POSTFIX
RUN yum --setopt=tsflags=nodocs install postfix -y && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN sed -i "s|#relayhost = uucphost|relayhost = mail.icmbio.gov.br|g" /etc/postfix/main.cf && \
    sed -i "s|inet_interfaces = localhost|#inet_interfaces = localhost|g" /etc/postfix/main.cf && \
    sed -i "s|#inet_interfaces = all|inet_interfaces = all|g" /etc/postfix/main.cf && \
    mkfifo /var/spool/postfix/public/pickup

RUN chkconfig postfix on
RUN postfix start

EXPOSE 443 80

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]

FROM production as testing
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>

# Instalacao do node para os testes
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -  && \
     yum install -y --setopt=tsflags=nodocs lynx net-tools git maven ant wget automake autoconf  dh-autoreconf nasm libjpeg-turbo-utils pngquant nodejs && \
     yum clean all && \
     rm -rf /var/cache/yum && \
     npm  install -g  yarn



CMD ["/run-httpd.sh"]
